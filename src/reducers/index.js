import auth from './auth_reducer';
import contact from './contact_reducer';

export default {
  auth,
  contact,
};
