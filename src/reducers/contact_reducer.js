import {CONTACT_LIST} from '../actions/types';

const INITIAL_STATE = {
  contactList: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONTACT_LIST:
      return {
        ...state,
        contactList: action.payload,
      };
    default:
      return state;
  }
};
