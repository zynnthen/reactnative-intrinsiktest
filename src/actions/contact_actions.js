import axios from 'axios';
import {CONTACT_LIST} from './types';
import {API_URL} from '@env';

export const getContacts = (callback) => async (dispatch) => {
  try {
    let url = `${API_URL}/contacts`;
    let {data} = await axios.get(url, null);
    dispatch({type: CONTACT_LIST, payload: data.data.contacts});
    callback(data, null);
  } catch (error) {
    console.warn(error);
    callback(null, error);
  }
};
