import axios from 'axios';
import {API_URL} from '@env';
import {TOKEN} from './types';
import AsyncStorage from '@react-native-community/async-storage';
import CONSTANTS from '../utils/constants';

export const login = (email, password, callback) => async (dispatch) => {
  try {
    let url = `${API_URL}/login`;
    let {data} = await axios.post(url, null);
    let token = data.data.token;
    await AsyncStorage.setItem(CONSTANTS.USER_TOKEN, token);
    dispatch({type: TOKEN, payload: token});
    callback({data: data, error: null});
  } catch (error) {
    console.error(error);
    callback({data: null, error: error});
  }
};

export const logout = (callback) => async (dispatch) => {
  try {
    let url = `${API_URL}/logout`;
    let {data} = await axios.post(url);
  } catch (error) {
    console.error(error);
  } finally {
    await AsyncStorage.removeItem(CONSTANTS.USER_TOKEN);
    dispatch({type: TOKEN, payload: null});
    callback();
  }
};
