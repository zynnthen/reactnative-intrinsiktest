import {API_URL} from '@env';

export function receiveMessage() {
  return fetch(`${API_URL}/receiveMessage`).then((data) => data.json());
}

export function sendMessage(obj) {
  return fetch(`${API_URL}/sendMessage`, {
    method: 'post',
    body: JSON.stringify(obj),
  }).then((data) => data.json());
}
