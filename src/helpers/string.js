export function isNullOrEmpty(text) {
  if (text === undefined || text === null) {
    return true;
  }

  const value = text.trim();
  return !value || value == undefined || value == '' || value.length == 0;
}

export function isValidEmailInput(text) {
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    return false;
  }
  return true;
}
