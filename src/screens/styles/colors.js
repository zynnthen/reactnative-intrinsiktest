export default {
  TEXT_HEADER: '#4B4B4B',
  TEXT_ERROR: '#FF0000',
  TEXT_PRIMARY: '#212121',
  TEXT_SECONDARY: '#727272',
  GRAY_100: '#9e9e9e',
  GRAY_200: '#d8d8d8',

  THEME: '#18a2b8',
};
