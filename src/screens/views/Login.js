import React, {Component} from 'react';
import {scale, verticalScale, moderateScale} from 'react-native-size-matters';
import {
  SafeAreaView,
  View,
  KeyboardAvoidingView,
  TextInput,
  Text,
  Platform,
  TouchableWithoutFeedback,
  Button,
  Keyboard,
  TouchableOpacity,
} from 'react-native';
import COLORS from '../styles/colors';
import styles from '../styles/styles';
import {connect} from 'react-redux';
import {login} from '../../actions';
import {isNullOrEmpty, isValidEmailInput} from '../..//helpers/string';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
    };
  }

  onLogin() {
    const {email, password} = this.state;
    if (isNullOrEmpty(email) || isNullOrEmpty(password)) {
      alert('Email and password cannot be empty');
      return;
    }
    if (!isValidEmailInput(email)) {
      alert('Invalid email input');
      return;
    }

    this.props.login(email, password, ({data, error}) => {
      if (error !== null) {
        return;
      }
      
      this.props.navigation.navigate('main');
    });
  }

  render() {
    return (
      <SafeAreaView style={[styles.container, customStyles.container]}>
        <KeyboardAvoidingView
          behavior={Platform.OS == 'ios' ? 'padding' : 'height'}
          style={styles.centerContainer}>
          <Text style={customStyles.appNameText}>Intrinsik OMS</Text>
          <Text style={styles.headerText}>Sign In</Text>
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <View>
              <Text style={customStyles.header}>Email</Text>
              <TextInput
                placeholder="Email"
                style={[styles.textInput, customStyles.textInputMargin]}
                keyboardType="email-address"
                autoCorrect={false}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                onChangeText={(text) => this.setState({email: text})}
                returnKeyType="next"
                onSubmitEditing={() => {
                  this.passwordTextInput.focus();
                }}
              />

              <Text style={customStyles.header}>Password</Text>
              <TextInput
                placeholder="Password"
                style={[styles.textInput, customStyles.textInputMargin]}
                secureTextEntry
                autoCorrect={false}
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({password: text})}
                ref={(input) => {
                  this.passwordTextInput = input;
                }}
              />

              <TouchableOpacity
                style={styles.btnContainer}
                onPress={() => {
                  this.onLogin();
                }}>
                <Text style={styles.btnContainerText}>Sign in</Text>
              </TouchableOpacity>

              <Button
                title="Sign up"
                color={COLORS.THEME}
                onPress={() => alert('Not implemented')}
              />
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaView>
    );
  }
}

const customStyles = {
  container: {
    margin: 20,
  },
  appNameText: {
    textAlign: 'center',
    color: '#026977',
    textShadowColor: 'rgba(0, 0, 0, 0.5)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 4,
    fontSize: moderateScale(24),
    fontWeight: 'bold',
    fontStyle: 'italic',
    marginBottom: verticalScale(16),
  },
  header: {
    color: COLORS.THEME,
    fontWeight: 'bold',
  },
  textInputMargin: {
    marginTop: 8,
    marginBottom: 8,
  },
};

const mapStateToProps = ({auth}) => {
  // const {user} = auth;
  // return {user};
};

export default connect(null, {login})(Login);
