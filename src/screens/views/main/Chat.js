import React, {useState, useCallback, useEffect, Button} from 'react';
import {receiveMessage, sendMessage} from '../../../actions';
import {connect} from 'react-redux';
import {GiftedChat} from 'react-native-gifted-chat';

export function Chat({navigation}) {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    let mounted = true;
    receiveMessage().then((data) => {
      if (mounted) {
        setMessages(data.data.messages);
      }
    });
    return () => (mounted = false);
  }, []);

  const onSend = useCallback((messages = []) => {
    setMessages((previousMessages) =>
      GiftedChat.append(previousMessages, messages),
    );

    sendMessage(messages[0]).then((data) => {
      setMessages((previousMessages) => {
        let newArr = [...previousMessages]; // copying the old datas array
        newArr[0].sent = true; // replace e.target.value with whatever you want to change it t
        return newArr;
      });
    });
  }, []);

  return (
    <GiftedChat
      messages={messages}
      onSend={(messages) => onSend(messages)}
      user={{
        _id: 1,
      }}
    />
  );
}

export default Chat;
