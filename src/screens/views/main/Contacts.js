import React, {Component} from 'react';
import {View, FlatList, Text, Button} from 'react-native';
import {connect} from 'react-redux';
import {logout, getContacts} from '../../../actions';
import ContactItem from '../../components/ContactItem';
import COLORS from '../../styles/colors';
import styles from '../../styles/styles';
import {NavigationActions} from 'react-navigation';

class ContactsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      contacts: [],
      error: null,
    };
  }

  static navigationOptions = ({navigation}) => {
    return {
      headerLeft: () => (
        <Button onPress={navigation.getParam('handleLogout')} title="Logout" />
      ),
    };
  };

  componentDidMount() {
    const {setParams} = this.props.navigation;
    setParams({
      handleLogout: this.onLogout.bind(this),
    });

    this.props.getContacts((response, error) => {
      this.setState({error: error});
    });
  }

  componentDidUpdate(prevProps) {
    if (this.props.contactList !== prevProps.contactList) {
      this.setState({contacts: this.props.contactList});
    }
  }

  onLogout() {
    this.props.logout(() => {
      // Sign-out successful.
      this.props.navigation.popToTop();
    });
  }

  onItemPressed(item) {
    this.props.navigation.navigate('Chat', {selectedItem: item});
  }

  handleRefresh = () => {
    this.setState({refreshing: true}, () => {
      this.props.getContacts((response, error) => {
        this.setState({error: error});
      });
      this.setState({refreshing: false});
    });
  };

  render() {
    const {error, contacts} = this.state;

    return error != null ? (
      <View style={styles.centerContainer}>
        <Text style={styles.errorText}>{error}</Text>
      </View>
    ) : (
      <View style={styles.container}>
        <FlatList
          data={contacts}
          keyExtractor={(item) => item.id.toString()}
          extraData={this.state}
          renderItem={({item}) => {
            return (
              <ContactItem
                item={item}
                onPress={(item) => this.onItemPressed(item)}
              />
            );
          }}
        />
      </View>
    );
  }
}

const mapStateToProps = ({contact}) => {
  const {contactList} = contact;
  return {contactList};
};

export default connect(mapStateToProps, {logout, getContacts})(ContactsScreen);
