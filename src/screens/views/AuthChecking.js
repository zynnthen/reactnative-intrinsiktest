import React from 'react';
import {StatusBar, StyleSheet, SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import CONSTANTS from '../../utils/constants';
import COLORS from '../styles/colors';
import styles from '../styles/styles';

class AuthChecking extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  componentDidMount() {
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      // The screen is focused
      // Call any action
      this._bootstrapAsync();
    });
  }

  componentWillUnmount() {
    // Remove the event listener
    this.focusListener.remove();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const token = await AsyncStorage.getItem(CONSTANTS.USER_TOKEN);

    // This will switch to the Main screen or Login screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(token != null ? 'main' : 'login');
  };

  // Render any loading content that you like here
  render() {
    return <SafeAreaView></SafeAreaView>;
  }
}

export default AuthChecking;
