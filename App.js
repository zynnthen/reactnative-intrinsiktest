/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import configureStore from './src/store';

import {
  StackActions,
  NavigationActions,
  createAppContainer,
} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import AuthChecking from './src/screens/views/AuthChecking';
import Login from './src/screens/views/Login';
import Contacts from './src/screens/views/main/Contacts';
import Chat from './src/screens/views/main/Chat';

import CONSTANTS from './src/utils/constants';

const AuthCheckNavigator = createStackNavigator({
  AuthChecking: {
    screen: AuthChecking,
    navigationOptions: {
      headerShown: false,
    },
  },
});

const LoginNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: {
      headerShown: false,
    },
  },
});

const MainNavigator = createStackNavigator({
  Contacts: {
    screen: Contacts,
  },
  Chat: {
    screen: Chat,
  },
});

const Container = createStackNavigator(
  {
    authLoad: {screen: AuthCheckNavigator},
    login: {screen: LoginNavigator},
    main: {screen: MainNavigator},
  },
  {
    headerMode: 'none',
    mode: 'modal',
    navigationOptions: {
      gesturesEnabled: false,
    },
    initialRouteName: 'authLoad',
  },
);

const {store} = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppContainer />
      </Provider>
    );
  }
}

const AppContainer = createAppContainer(Container);
